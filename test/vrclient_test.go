package test

import (
	"bytes"
	"fmt"
	"testing"

	"gitee.com/spaty-zhixai/minitools/njson"
	"gitee.com/spaty-zhixai/minitools/text"
	"gitee.com/spaty-zhixai/vrclient"
)

type Message struct {
	Msg  string `json:"msg"`
	Code int    `json:"code"`
}

func TestSetGlobalFunc(t *testing.T) {
	fmt.Println(vrclient.DefaultClient)
	vrclient.SetGlobalParseFunc(func(btlk vrclient.BytesLake, v interface{}) error {
		return nil
	})
	fmt.Println(vrclient.DefaultClient)
	vrclient.Reset()
	fmt.Println(vrclient.DefaultClient)
}

func TestPost(t *testing.T) {
	url := "http://chifan.taimei.com/login"
	var msg Message
	p := make(map[string]string, 0)
	p["username"] = ""
	p["password"] = ""
	p["rememberMe"] = "false"
	bts, _ := njson.Marshal(p)
	blk := vrclient.
		Timeout(5*1000).
		Header("Content-Type", "application/json").
		Post(url, bytes.NewReader(bts))
	err := blk.ParseResult(&msg)
	if err != nil {
		fmt.Printf("%d:%s", blk.StatusCode(), err.Error())
		return
	}
	fmt.Println("msg:", msg)
	fmt.Println("status-code:", blk.StatusCode())
	fmt.Println("cookies:", blk.Cookies())
	vrclient.SetCookies("http://chifan.taimei.com/")
}

func TestDownload(t *testing.T) {
	url := "http://www.baidu.com"
	err := vrclient.
		Timeout(50*1000).
		Get(url).
		Download("tmp", "baidu.html")
	fmt.Println(err)
}

func TestHtml(t *testing.T) {
	url := "http://www.bing.com"
	blk, bts, err := vrclient.
		Timeout(5*1000).
		Header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3").
		Header("Accept-Language", "zh-CN,zh;q=0.9").
		Header("Content-Type", "text/html;charset=utf-8").
		Header("Content-Type", "text/html").
		Html(url)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("zip-type:", blk.ZipType())
	fmt.Println("html:", text.ToString(bts))
	fmt.Println("cookies:", blk.Cookies())
}

func BenchmarkGet(t *testing.B) {
	url := "http://www.baidu.com"
	_, _, err := vrclient.
		Timeout(5*1000).
		SetCookies(url).
		Header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3").
		Header("Accept-Language", "zh-CN,zh;q=0.9").
		Header("Content-Type", "text/html;charset=utf-8").
		Html(url)
	if err != nil {
		fmt.Println(err)
		return
	}
	// fmt.Println(bts)
}
