// import "gitee.com/spaty-zhixai/minitools/conversion"
package conversion

import (
	"reflect"
	"strconv"
)

// 将int转换为string
// 将int64转换为string
// 将float64转换为string(bitSize = 64)
// 将float32转换为string(bitSize = 32)
func ToString[Number ~int | ~int64 | ~float32 | ~float64 | ~bool | ~[]uint8](n Number) string {
	v := reflect.ValueOf(n)
	switch kind := v.Kind(); kind {
	case reflect.Int:
		return strconv.Itoa(int(v.Int()))
	case reflect.Int64:
		return strconv.FormatInt(v.Int(), 10)
	case reflect.Float32:
		return strconv.FormatFloat(v.Float(), 'E', -1, 32)
	case reflect.Float64:
		return strconv.FormatFloat(v.Float(), 'E', -1, 64)
	case reflect.Bool:
		return boolToString(v.Bool())
	case reflect.Slice | reflect.Array:
		var (
			bts []byte
		)
		for i := 0; i < v.Len(); i++ {
			iv := v.Index(i)
			if iv.Kind() == reflect.Uint8 {
				bts = append(bts, byte(iv.Uint()))
			}
		}
		if len(bts) > 0 {
			return string(bts)
		}
	}
	return ""
}

// 将string转换成[]uint8
func ToUnit8[String ~string](s String) (u8arr []uint8) {
	bts := ToByte(s)
	for _, b := range bts {
		u8arr = append(u8arr, uint8(b))
	}
	return u8arr
}

// 将string转[]byte
func ToByte[String ~string](s String) []byte {
	return []byte(s)
}

// 将string转换为bool
func ToBool[String ~string](s String) (bool, error) {
	return strconv.ParseBool(string(s))
}

// 将bool或字符转换为int
func ToInt[StringOrBool ~bool | ~string](bs StringOrBool) (int, error) {
	v := reflect.ValueOf(bs)
	switch kind := v.Kind(); kind {
	case reflect.String:
		return strconv.Atoi(v.String())
	case reflect.Bool:
		switch v.Bool() {
		case true:
			return int(1), nil
		default:
			return int(0), nil
		}
	}
	return -1, nil
}

// 将string转换为int?
// bitSize = 64
// 十进制数值
func ToInt64[String ~string](s String) (int64, error) {
	return strconv.ParseInt(string(s), 10, 64)
}

// 将string转换为float
// bitSize = 64
func ToFloat64[String ~string](s String) (float64, error) {
	return strconv.ParseFloat(string(s), 64)
}

// bool转成字符串
func boolToString(b bool) string {
	switch b {
	case true:
		return "true"
	default:
		return "false"
	}
}
