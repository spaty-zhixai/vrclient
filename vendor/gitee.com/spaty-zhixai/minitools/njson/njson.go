// import "gitee.com/spaty-zhixai/minitools/njson"
package njson

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"strings"

	"gitee.com/spaty-zhixai/minitools/text"
)

func Unmarshal(bts []byte, v interface{}) error {
	rd := bytes.NewReader(bts)
	dc := json.NewDecoder(rd)

	err := unmarshalUseNumber(dc, v)
	if err != nil {
		return fmtError(text.ToString(bts), err)
	}
	return nil
}

func UnmarshalReader(rd io.Reader, v interface{}) error {
	var buf strings.Builder
	teeRd := io.TeeReader(rd, &buf)
	dc := json.NewDecoder(teeRd)

	err := unmarshalUseNumber(dc, v)
	if err != nil {
		return fmtError(buf.String(), err)
	}
	return nil
}

func UnmarshalString(s string, v interface{}) error {
	rd := text.ToReader(s)
	dc := json.NewDecoder(rd)

	err := unmarshalUseNumber(dc, v)
	if err != nil {
		return fmtError(s, err)
	}
	return nil
}

func Marshal(v interface{}) ([]byte, error) {
	return json.Marshal(v)
}

func MarshalTabIndent(v interface{}) ([]byte, error) {
	return json.MarshalIndent(v, "", "	")
}

func fmtError(s string, err error) error {
	return fmt.Errorf("{'json':'%s', 'error':'%s'}", s, err.Error())
}

func unmarshalUseNumber(dcoder *json.Decoder, v interface{}) error {
	dcoder.UseNumber()
	return dcoder.Decode(v)
}
