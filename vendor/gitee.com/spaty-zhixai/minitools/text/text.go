// 处理字符串的工具
// import "gitee.com/spaty-zhixai/minitools/text"
package text

import (
	"crypto/md5"
	"encoding/hex"
	"io"
	"strings"
	"unicode/utf8"

	"gitee.com/spaty-zhixai/minitools/conversion"
)

const (
	OFF_SET = 2147483647
)

// 获取字符串的长度
func StrLength(s string) int {
	if s == "" {
		return 0
	}
	sr := []rune(s)
	return len(sr)
}

//获取字符串中某个字符的位置，若不存在则返回-1
func IndexOfIgnoreCase(s, substr string) int {
	if len(substr) == 0 {
		return 0
	}

	s = strings.ToLower(s)
	substr = strings.ToLower(substr)

	return IndexOf(s, substr)
}

func IndexOf(s, substr string) int {
	substrn := []rune(substr) //语言不同转换 utf-8
	l := len(substrn)         //某个字符的长度
	if l == 0 {
		return 0
	}

	sr := []rune(s)
	n := len(sr) // 字符串的长度
	m := 0       // 字符串的开始位置
	begin := sr[m]

	if l >= 1 { // 子字符串长度大于1时
		n = len(sr) + 1 // 将字符串的长度+1
		m = l           // 某个字段串的末位值赋于开始位置
	}

	for i := 0; i+m < n; i++ {
		if m == 0 {
			if strings.Compare(string(sr[i]), string(begin)) == 0 {
				return i
			}
		} else {
			if strings.Compare(string(sr[i:i+m]), substr) == 0 {
				return i
			}
		}
	}
	return -1
}

//判断是否包含此字符串
func Contains(s, substr string) bool {
	return IndexOf(s, substr) != -1
}

// 获取字符串的最后一个字符及截取最后一个字符剩余的内容
func LastChar(s string) ([]uint8, string) {
	if s == "" {
		panic("String cannot be empty.")
	}
	sr := []rune(s)
	ns := string(sr[len(sr)-1])
	lns := string(sr[:len(sr)-1])
	return conversion.ToUnit8(ns), lns
}

// 获取字符串的第一个字符
func FristChar(s string) (u8arr []uint8) {
	if s == "" {
		return
	}
	sr := []rune(s)
	ns := string(sr[0])
	return conversion.ToUnit8(ns)
}

// 判断字符是否某个字符串的开始
func BeginsWith(s, begins string) bool {
	sr := []rune(s)
	bsr := []rune(begins)
	return len(sr) >= len(bsr) && string(sr[0:len(bsr)]) == begins
}

// MD5- 32位小写
func MD5(s string) string {
	b := []byte(s)
	m := md5.Sum(b)
	return hex.EncodeToString(m[:])
}

// HashCode MD5之后的HashCode
func HashCode(md5 string) int {
	md5 = MD5(md5)
	hc := 0
	rs := []rune(md5)
	for i := 0; i < len(rs); i++ {
		hc = 31*hc + int(rs[i])
		if hc >= OFF_SET {
			hc %= OFF_SET
		}
	}
	return hc
}

// 将[]byte转换为string
func ToString[Bytes ~[]byte](bs Bytes) string {
	return string(bs)
}

//截取字符串
func SubStr(s string, begin, length int) string {
	if s == "" {
		return ""
	}
	if begin < 0 {
		begin = 0
	}
	strR := []rune(s)
	l := utf8.RuneCountInString(s)
	if begin >= l {
		return ""
	}
	if length > l {
		return string(strR[begin:])
	}
	return string(strR[begin : begin+length])
}

// 将字符串转成Reader
func ToReader[String ~string](s String) io.Reader {
	return strings.NewReader(string(s))
}
