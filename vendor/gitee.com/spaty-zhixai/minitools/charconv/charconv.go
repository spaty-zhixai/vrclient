// import "gitee.com/spaty-zhixai/minitools/charconv"
package charconv

import (
	"gitee.com/spaty-zhixai/minitools/text"
	"golang.org/x/net/html/charset"
	"golang.org/x/text/encoding"
	"golang.org/x/text/encoding/japanese"
	"golang.org/x/text/encoding/korean"
	"golang.org/x/text/encoding/simplifiedchinese"
	"golang.org/x/text/encoding/traditionalchinese"
	"golang.org/x/text/encoding/unicode"
)

type (
	ncharset struct {
		name      Charset
		decoder   *encoding.Decoder
		encoder   *encoding.Encoder
		htmlBytes []byte
	}
	Charset string
)

const (
	UTF8      = Charset("UTF-8")
	GBK       = Charset("GBK")
	GB18030   = Charset("GB18030")
	GB2312    = Charset("GB2312")
	BIG5      = Charset("BIG5")
	EUCKR     = Charset("EUC-KR")
	EUCJP     = Charset("EUC-JP")
	ISO2022JP = Charset("ISO-2022-JP")
	ShiftJIS  = Charset("Shift JIS")
)

// 获取 html document charset
func HtmlCharset(byts []byte) *ncharset {
	ec, nm, _ := charset.DetermineEncoding(byts, "")
	cs := &ncharset{}
	cs.htmlBytes = byts
	cs.name = Charset(nm)
	cs.decoder = ec.NewDecoder()
	cs.encoder = ec.NewEncoder()
	return cs
}

// 获取对应的编码集
func WithCharset(n Charset) *ncharset {
	cs := &ncharset{}
	cs.name = n
	switch cs.name {
	case GBK:
		cs.encoder = simplifiedchinese.GBK.NewEncoder()
		cs.decoder = simplifiedchinese.GBK.NewDecoder()
		break
	case GB18030:
		cs.encoder = simplifiedchinese.GB18030.NewEncoder()
		cs.decoder = simplifiedchinese.GB18030.NewDecoder()
		break
	case GB2312:
		cs.encoder = simplifiedchinese.HZGB2312.NewEncoder()
		cs.decoder = simplifiedchinese.HZGB2312.NewDecoder()
		break
	case BIG5:
		cs.encoder = traditionalchinese.Big5.NewEncoder()
		cs.decoder = traditionalchinese.Big5.NewDecoder()
		break
	case EUCKR:
		cs.encoder = korean.EUCKR.NewEncoder()
		cs.decoder = korean.EUCKR.NewDecoder()
		break
	case EUCJP:
		cs.encoder = japanese.EUCJP.NewEncoder()
		cs.decoder = japanese.EUCJP.NewDecoder()
		break
	case ISO2022JP:
		cs.encoder = japanese.ISO2022JP.NewEncoder()
		cs.decoder = japanese.ISO2022JP.NewDecoder()
		break
	case ShiftJIS:
		cs.encoder = japanese.ShiftJIS.NewEncoder()
		cs.decoder = japanese.ShiftJIS.NewDecoder()
		break
	default:
		cs.encoder = unicode.UTF8.NewEncoder()
		cs.decoder = unicode.UTF8.NewDecoder()
		break
	}
	return cs
}

// html解码
func (cs *ncharset) HtmlDecode() (string, error) {
	return decodeBytesToString(cs.decoder, cs.htmlBytes)
}

// html decode to bytes
func (cs *ncharset) HtmlDecodeToBytes() ([]byte, error) {
	return decodeBytes(cs.decoder, cs.htmlBytes)
}

// 解码成字符串
func (cs *ncharset) DecodeBytesToStr(bts []byte) (string, error) {
	return decodeBytesToString(cs.decoder, bts)
}

// decode to bytes
func (cs *ncharset) DecodeBytes(bts []byte) ([]byte, error) {
	return decodeBytes(cs.decoder, bts)
}

// 编码成字节
func (cs *ncharset) EncodeBytes(bts []byte) ([]byte, error) {
	return encodeBytes(cs.encoder, bts)
}

// private decode bytes to string
func decodeBytesToString(decoder *encoding.Decoder, bts []byte) (string, error) {
	nbts, err := decodeBytes(decoder, bts)
	if err != nil {
		return "", err
	}
	return text.ToString(nbts), nil
}

// private decode bytes to bytes
func decodeBytes(decoder *encoding.Decoder, bts []byte) ([]byte, error) {
	nbts, err := decoder.Bytes(bts)
	if err != nil {
		return nil, err
	}
	return nbts, nil
}

// private encode bytes to bytes
func encodeBytes(encoder *encoding.Encoder, bts []byte) ([]byte, error) {
	nbts, err := encoder.Bytes(bts)
	if err != nil {
		return nil, err
	}
	return nbts, nil
}
