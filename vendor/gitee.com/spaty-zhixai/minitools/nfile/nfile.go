// import "gitee.com/spaty-zhixai/minitools/nfile"
package nfile

import (
	"bufio"
	"bytes"
	"encoding/hex"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"time"

	"gitee.com/spaty-zhixai/minitools/conversion"
)

type (
	fileInfo struct {
		Name       string
		Size       int64
		Mode       string
		ModifyDate time.Time
	}
	BufferFileReader struct {
		*bufio.Reader
	}
)

var FileHeaderMap = map[string]string{
	"ffd8ff":                       "JPEG",     // 3 bytes
	"89504e47":                     "PNG",      // 4 bytes
	"47494638":                     "GIF",      // 4 bytes
	"504b0304":                     "ZIP",      // 4 bytes
	"52617221":                     "RAR",      // 4 bytes
	"377abcaf271c":                 "7z",       // 6 bytes
	"49492a00":                     "TIFF",     // 4 bytes
	"424d":                         "BMP",      // 2 bytes
	"41433130":                     "DWG",      // 4 bytes
	"38425053":                     "PSD",      // 4 bytes
	"7b5c727466":                   "RTF",      // 5 bytes
	"3c3f786d6c":                   "XML",      // 5 bytes
	"68746d6c3e":                   "HTML",     // 5 bytes
	"44656c69766572792d646174653a": "EML",      // 14 bytes
	"cfad12fec5fd746f":             "DBX",      // 8 bytes
	"2142444e":                     "PST",      // 4 bytes
	"d0cf11e0":                     "OFFICE97", // 4 bytes
	"504b030414000600080000002100": "OFFICE",   // 14 bytes
	"5374616e64617264204a":         "MDB",      // 10 bytes
	"ff575043":                     "WPD",      // 4 bytes
	"255044462d312e":               "PDF",      // 7 bytes
	"ac9ebd8f":                     "QDF",      // 4 bytes
	"e3828596":                     "PWL",      // 4 bytes
	"57415645":                     "WAV",      // 4 bytes
	"41564920":                     "AVI",      // 4 bytes
	"2e7261fd":                     "RAM",      // 4 bytes
	"2e524d46":                     "RM",       // 4 bytes
	"000001ba":                     "MPG",      // 4 bytes
	"000001b3":                     "MPG",      // 4 bytes
	"6d6f6f76":                     "MOV",      // 4 bytes
	"3026b2758e66cf11":             "ASF",      // 8 bytes
	"4d546864":                     "MID",      // 4 bytes
	"d0cf11e0a1b11ae10000":         "WPS",      // 10 bytes
	"00000020667479706d70":         "MP4",      // 10 bytes
	"49443303000000002176":         "MP3",      // 10 bytes
	"49545346030000006000":         "CHM",      // 10 bytes
	"4d5a9000030000000400":         "EXE",      // 10 bytes
}

// 读取文件，返回io.Reader
func Read(filename string) *os.File {
	f, err := os.Open(filename)
	if err != nil {
		fmt.Print(err.Error())
		return nil
	}
	return f
}

// 返回读取并可写入的io.Reader
func ReadOrWrite(filename string) *os.File {
	of, err := os.OpenFile(filename, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0766)
	if err != nil {
		fmt.Print(err.Error())
		return nil
	}
	return of
}

// 创建文件夹，不存在才创建
func NewFolder(folder string) error {
	err := os.MkdirAll(folder, os.ModePerm)
	return err
}

// 返回程序运行时的根路径
func RootPath() string {
	root, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		fmt.Print(err.Error())
		return ""
	}
	return root
}

// 构造一个新的buffer Reader
func NewBufferFileReader(r io.Reader) *BufferFileReader {
	return &BufferFileReader{bufio.NewReader(r)}
}

// 分片读取固定字节的数据
func ReadFixBytes(br *BufferFileReader, buf []byte) error {
	if br == nil {
		panic("buffer reader is missing.")
	}
	if _, err := br.Read(buf); err != nil {
		return err
	}
	return nil
}

// 获取文件的信息
func FileInfo(name string) *fileInfo {
	nf, err := os.Stat(name)
	if err != nil {
		panic(err)
	}
	if nf.IsDir() {
		panic(name + " is a directory")
	}
	return &fileInfo{
		Name:       nf.Name(),
		Size:       nf.Size(),
		Mode:       nf.Mode().String(),
		ModifyDate: nf.ModTime(),
	}
}

// 获取文件头信息
// offset是文件头占有的字节长度
func FileHeader(br *BufferFileReader, offset int) string {
	if br == nil || offset < 2 {
		return ""
	}
	bf := bytes.NewBuffer(nil)
	for i := 0; i < offset; i++ {
		bt, err := br.ReadByte()
		if err != nil {
			panic(err)
		}
		v := bt & 0xFF
		hv := hex.EncodeToString(append([]byte{}, v))
		if len(hv) < 2 {
			// 补0
			bf.WriteString(conversion.ToString(int64(0)))
		}
		bf.WriteString(hv)
	}

	fHeader := bf.String()
	bf.Reset()

	if h, ok := FileHeaderMap[fHeader]; ok {
		return h
	}
	return fHeader
}
