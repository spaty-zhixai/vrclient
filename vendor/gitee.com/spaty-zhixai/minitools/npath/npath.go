package npath

import (
	"os"
	"path"

	"gitee.com/spaty-zhixai/minitools/conversion"
	"gitee.com/spaty-zhixai/minitools/text"
)

const (
	DIAGONAL    string = "/"
	DEFULT_PORT string = ":8080"
)

// 合并路径
func Joint(paths ...string) string {
	return path.Join(paths...)
}

// 在绝对路径absPath后追加相对路径realPath
func ResolvePath(absPath, relaPath string) string {
	// 当相对路径为空，则返回绝对路径
	if relaPath == "" {
		return absPath
	}

	// 根据绝对路径和相对路径合并成一个新路径
	nPath := Joint(absPath, relaPath)
	// 处理新路径
	if CheckEndForward(relaPath) && !CheckEndForward(nPath) {
		nPath = nPath + DIAGONAL
	}
	return nPath
}

// 检查是否为"/"结尾
func CheckEndForward(path string) bool {
	l := len(path)
	lc, _ := text.LastChar(path)
	if l > 1 && conversion.ToString(lc) == DIAGONAL {
		return true
	}
	return false
}

// 去掉URL末尾的“/”
func CleanURL(url *string) {
	l := len(*url)
	if l > 1 {
		if (*url)[l-1:] == DIAGONAL {
			*url = (*url)[:l-1]
			CleanURL(url)
		}
	}
}

// 处理端口号
func ResolveAddr(addr, env string) string {
	if addr == "" {
		port := defaultPort(env)

		if !text.BeginsWith(port, ":") {
			port = ":" + port
		}
		return port
	}

	if !text.BeginsWith(addr, ":") {
		addr = ":" + addr
	}
	return addr
}

// 默认从指定的环境变量名中获取;
// 未获取到，则返回默认端口8080
func defaultPort(env string) (port string) {
	port = DEFULT_PORT
	if env != "" {
		if port = os.Getenv(env); port == "" {
			port = DEFULT_PORT
		}
	}
	return
}
