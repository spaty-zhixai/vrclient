module gitee.com/spaty-zhixai/vrclient

go 1.18

replace (
	golang.org/x/net => github.com/golang/net v0.0.0-20200707034311-ab3426394381
	golang.org/x/text => github.com/golang/text v0.3.4
)

require gitee.com/spaty-zhixai/minitools v0.0.0-20220812033800-3ffb5d2310d0

require (
	golang.org/x/net v0.0.0-20190404232315-eb5bcb51f2a3 // indirect
	golang.org/x/text v0.3.4 // indirect
)
