package vrclient

import (
	"bufio"
	"bytes"
	"compress/gzip"
	"errors"
	"io"
	"net/http"
	"strings"

	"gitee.com/spaty-zhixai/minitools/nfile"
	"gitee.com/spaty-zhixai/minitools/npath"
)

type (
	BytesLake interface {
		// zip type
		ZipType() string
		// error
		Error() error
		// response code
		StatusCode() int
		// response http.Header
		HttpHeader() http.Header
		// get header value by key
		ResponseHeader(key string) string
		// get result
		Result() ([]byte, error)
		// get decode result
		DecodeResult(dcf DecodeBytesFunc) ([]byte, error)
		// parse result
		ParseResult(v any) error
		// parse result with custom func
		ParseResultWithFunc(v any, prf ParseResultFunc) error
		// Download file
		Download(directory, filename string) error
		// get cookies
		Cookies() []*http.Cookie
	}

	bytesLake struct {
		internalError  error
		client         *vrClient
		responseCode   int
		responseHeader http.Header
		resultBody     io.ReadCloser
		cookies        []*http.Cookie
		zipType        string
	}
	DecodeBytesFunc func([]byte) ([]byte, error)
)

const GZIP string = "gzip"

// 解压gzip压缩字节流
func (btslak *bytesLake) gzipDecodeBody() (*gzip.Reader, error) {

	isGzip := strings.EqualFold(btslak.zipType, GZIP)
	if isGzip {
		gr, err := gzip.NewReader(btslak.resultBody)

		if err != nil {
			btslak.internalError = err
		}
		return gr, nil
	}
	return nil, nil
}

func newBytesLake(vrc *vrClient) *bytesLake {
	btslk := &bytesLake{
		client: vrc,
	}
	return btslk
}

func (btslak *bytesLake) error(err error) *bytesLake {
	btslak.internalError = err
	return btslak
}

func (btslak *bytesLake) rCode(rcode int) *bytesLake {
	btslak.responseCode = rcode
	return btslak
}

func (btslak *bytesLake) rHeader(rh http.Header) *bytesLake {
	btslak.responseHeader = rh

	if btslak.internalError == nil {
		// set cookies
		btslak.cookies = btslak.client.httpClient.Jar.Cookies(btslak.client.askRequest.URL)
		// set zip type
		if rh != nil {
			btslak.zipType = rh.Get("Content-Encoding")
		}
	}
	return btslak
}

func (btslak *bytesLake) rBody(rb io.ReadCloser) *bytesLake {
	btslak.resultBody = rb
	return btslak
}

// private use parse-func to parse bytes
func (btslak *bytesLake) parserResult(parseResult any, parseFunc ParseResultFunc) error {
	if btslak.internalError != nil {
		return btslak.internalError
	}
	if parseResult != nil && parseFunc != nil {
		return parseFunc.ParserResult(btslak, parseResult)
	}
	if parseResult != nil && btslak.client.globalParseFunc != nil {
		return btslak.client.globalParseFunc.ParserResult(btslak, parseResult)
	}
	return nil
}

// initial result object
func (btslak *bytesLake) ParseResult(result any) error {
	return btslak.parserResult(result, nil)
}

// initial result object and parse func
func (btslak *bytesLake) ParseResultWithFunc(result any, parseFunc ParseResultFunc) error {
	return btslak.parserResult(result, parseFunc)
}

var _ BytesLake = (*bytesLake)(nil)

// internal error
func (btslak *bytesLake) Error() error {
	return btslak.internalError
}

// response header
func (btslak *bytesLake) HttpHeader() http.Header {
	return btslak.responseHeader
}

// get response header with key
func (btslak *bytesLake) ResponseHeader(key string) string {
	if btslak.responseHeader != nil {
		return btslak.responseHeader.Get(key)
	}
	return ""
}

// get response code
func (btslak *bytesLake) StatusCode() int {
	return btslak.responseCode
}

// get body result
func (btslak *bytesLake) Result() ([]byte, error) {
	if err := btslak.setGZipBody(); err != nil {
		return nil, err
	}

	if btslak.resultBody != nil {
		defer btslak.resultBody.Close()
	}

	// 读取body缓冲区
	buf := dynamicBuffer(512)
	var buffer bytes.Buffer

	// 缓冲读取流
loop:
	for {
		n, err := btslak.resultBody.Read(buf)

		if n > 0 {
			buffer.Write(buf[:n])
		}

		// 读取完毕，跳出loop
		if err == io.EOF {
			break loop
		}
		// 发生错误，则返回异常
		if err != nil {
			btslak.internalError = err
			return nil, err
		}

	}

	bts := buffer.Bytes()
	buffer.Reset()

	return bts, nil
}

// 下载文件
func (btslak *bytesLake) Download(directory, filename string) error {

	if err := btslak.setGZipBody(); err != nil {
		return err
	}

	if btslak.resultBody != nil {
		defer btslak.resultBody.Close()
	}

	if directory == "" || filename == "" {
		return errors.New("The download path is not specified.")
	}

	dPath := npath.ResolvePath(nfile.RootPath(), directory)

	if err := nfile.NewFolder(dPath); err != nil {
		return err
	}

	fPath := npath.ResolvePath(dPath, filename)
	f := nfile.ReadOrWrite(fPath)

	if f == nil {
		return errors.New("The download path is missing.")
	}
	defer f.Close()
	// 读取body缓冲区
	buf := dynamicBuffer(32 * 1024)
	write := bufio.NewWriter(f)

loop:
	for {
		n, err := btslak.resultBody.Read(buf)

		if n > 0 {
			_, err = write.Write(buf[:n])
		}
		// 读取完毕，跳出loop
		if err == io.EOF {
			break loop
		}
		// 发生错误，则返回异常
		if err != nil {
			btslak.internalError = err
			return err
		}
	}
	if err := write.Flush(); err != nil {
		return err
	}
	return nil
}

// get cookies
func (btslak *bytesLake) Cookies() (cookies []*http.Cookie) {
	return btslak.cookies
}

// get decode result
func (btslak *bytesLake) DecodeResult(dcf DecodeBytesFunc) ([]byte, error) {
	bts, err := btslak.Result()
	if err != nil {
		return nil, err
	}
	return dcf(bts)
}

// 构造动态缓冲区
func dynamicBuffer(size int) (buf []byte) {
	buf = make([]byte, size)
	return
}

// zip type
func (btslak *bytesLake) ZipType() string {
	return btslak.zipType
}

func (btslak *bytesLake) setGZipBody() error {
	if (btslak.internalError != nil && btslak.resultBody == nil) || btslak.resultBody == nil {
		return btslak.internalError
	}

	gb, err := btslak.gzipDecodeBody()

	if err != nil {
		btslak.internalError = err
		return err
	}

	if gb != nil {
		btslak.resultBody = gb
	}
	return nil
}
