// import "gitee.com/spaty-zhixai/vrclient"
package vrclient

import (
	"bytes"
	"errors"
	"io"
	"mime/multipart"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strings"
	"time"
	"unicode"

	"gitee.com/spaty-zhixai/minitools/charconv"
	"gitee.com/spaty-zhixai/minitools/njson"
)

type (
	vrClient struct {
		askRequest      *http.Request
		httpClient      *http.Client
		headers         vheader
		globalParseFunc ParseResultFunc
	}
	// Params
	Query struct {
		Key   string
		Value string
	}
	// upload file
	UploadFile struct {
		// file name
		Name string
		// full file path
		FileName string
	}
	//vheader 请求头
	vheader map[string]string
	//JarCookie Cookie
	JarCookie struct {
		cookies []*http.Cookie
		vurl    *url.URL
	}
	// parse result func
	ParseResultFunc func(BytesLake, any) error
	// 结果处理接口
	ResultHandler interface {
		ParserResult(BytesLake, any) error
	}
)

// call parse result func 入口
func (parseFunc ParseResultFunc) ParserResult(btlk BytesLake, v any) error {
	return parseFunc(btlk, v)
}

//New 初始化一个新的VRClient
func newVRClient() *vrClient {
	vrc := &vrClient{
		httpClient:      newClient(),
		headers:         make(vheader, 0),
		globalParseFunc: defaultParseToJson,
	}
	// 默认请求头
	vrc.Header("User-Agent", "VR-Client v1.0.2020").
		Header("Accept", "application/json").
		Header("Accept-Encoding", "gzip, deflate")

	return vrc
}

//Header 设置请求头
func (vrc *vrClient) Header(key, value string) *vrClient {
	vrc.headers[standardizeKey(key)] = value
	return vrc
}

//Timeout 设置超时，单位毫秒
func (vrc *vrClient) Timeout(timeout int) *vrClient {
	vrc.httpClient.Timeout = time.Duration(time.Duration(timeout) * time.Millisecond)
	return vrc
}

// set cookie
// if cookie is nil, the cookies is set to empty
func (vrc *vrClient) SetCookies(rawUrl string, ck ...*http.Cookie) *vrClient {
	u, err := url.Parse(rawUrl)
	if err != nil {
		panic(err)
	}
	if len(ck) > 0 {
		cks := make([]*http.Cookie, 0)
		cks = append(cks, ck...)
		vrc.httpClient.Jar.SetCookies(u, cks)
	} else {
		vrc.httpClient.Jar.SetCookies(u, nil)
	}

	return vrc
}

//POST POST请求
func (vrc *vrClient) Post(url string, body io.Reader) BytesLake {
	return execute(vrc, vrc.buildRequest("POST", url, body))
}

//GET GET请求
func (vrc *vrClient) Get(url string, query ...*Query) BytesLake {
	q := buildQueries(query...)
	return execute(vrc, vrc.buildRequest("GET", url, q))
}

// DELETE DELETE请求
func (vrc *vrClient) Delete(url string, query ...*Query) BytesLake {
	q := buildQueries(query...)
	return execute(vrc, vrc.buildRequest("DELETE", url, q))
}

// PUT PUT请求
func (vrc *vrClient) Put(url string, query ...*Query) BytesLake {
	q := buildQueries(query...)
	return execute(vrc, vrc.buildRequest("PUT", url, q))
}

// Get html document
func (vrc *vrClient) Html(url string) (BytesLake, []byte, error) {
	btlk := vrc.Get(url)
	bts, err := btlk.DecodeResult(DecodeBytesFunc(func(bs []byte) ([]byte, error) {
		return charconv.HtmlCharset(bs).HtmlDecodeToBytes()
	}))
	if err != nil {
		return btlk, nil, err
	}
	return btlk, bts, err

}

// upload error
func (vrc *vrClient) UploadError(err error) BytesLake {
	return newBytesLake(vrc).
		error(err).
		rCode(500)
}

//SetCookies 设置Cookies
func (jar *JarCookie) SetCookies(u *url.URL, cookies []*http.Cookie) {
	jar.vurl = u
	jar.cookies = cookies
}

//Cookies 获取Cookies
func (jar *JarCookie) Cookies(u *url.URL) []*http.Cookie {
	if jar.vurl == nil || u.Host == jar.vurl.Host {
		return jar.cookies
	}
	return nil
}

// 新建一个http client
func newClient() *http.Client {
	return &http.Client{
		Jar:     &JarCookie{},
		Timeout: time.Second,
	}
}

// 获取请求响应
func (vrc *vrClient) buildRequest(method, url string, body io.Reader) *http.Request {

	req, err := http.NewRequest(method, url, body)
	if err != nil {
		panic(err.Error())
	}

	for k, v := range vrc.headers {
		req.Header.Set(k, v)
	}
	return req
}

// 处理响应
func execute(vrc *vrClient, req *http.Request) BytesLake {

	resp, err := vrc.httpClient.Do(req)
	vrc.askRequest = req

	if err != nil {
		return newBytesLake(vrc).
			error(err).
			rCode(http.StatusInternalServerError)
	}

	var (
		respErr  error
		respCode int
	)
	respCode = http.StatusOK

	if resp.StatusCode != http.StatusOK {
		respErr = errors.New(resp.Status)
		respCode = resp.StatusCode
	}

	return newBytesLake(vrc).
		rCode(respCode).
		rHeader(resp.Header).
		rBody(resp.Body).
		error(respErr)
}

// reset client to initial
func (vrc *vrClient) ResetWiths(parseFunc func(BytesLake, any) error) {
	if parseFunc != nil {
		vrc.globalParseFunc = ParseResultFunc(parseFunc)
		return
	}
	if GlobalParseFunc != nil {
		vrc.globalParseFunc = GlobalParseFunc
		return
	}
	vrc.globalParseFunc = defaultParseToJson
}

// default parse result to json
func defaultParseToJson(btslak BytesLake, v any) error {
	bts, err := btslak.Result()
	if err != nil {
		return err
	}
	return njson.Unmarshal(bts, v)
}

// standardize key
func standardizeKey(n string) string {
	var buf bytes.Buffer
	for _, rn := range n {
		switch {
		case unicode.IsDigit(rn):
			buf.WriteRune(rn)
		case unicode.IsLetter(rn):
			buf.WriteRune(unicode.ToLower(rn))
		default:
			buf.WriteRune(rn)
		}
	}
	return buf.String()
}

// build query params
func buildQueries(query ...*Query) io.Reader {
	urlVals := url.Values{}
	for _, v := range query {
		urlVals.Set(v.Key, v.Value)
	}
	return strings.NewReader(urlVals.Encode())
}

// reset client to initial
func Reset() {
	DefaultClient.ResetWiths(nil)
}

// default VR Client
var DefaultClient = newVRClient()

// Post
func Post(url string, body io.Reader) BytesLake {
	return DefaultClient.Post(url, body)
}

// Get
func Get(url string, query ...*Query) BytesLake {
	return DefaultClient.Get(url, query...)
}

// Put
func Put(url string, query ...*Query) BytesLake {
	return DefaultClient.Put(url, query...)
}

// Delete
func Delete(url string, query ...*Query) BytesLake {
	return DefaultClient.Delete(url, query...)
}

// add header
func Header(key, value string) *vrClient {
	return DefaultClient.Header(key, value)
}

// global parse func
var GlobalParseFunc ParseResultFunc

// set global parse func
func SetGlobalParseFunc(parseFunc func(BytesLake, any) error) {
	GlobalParseFunc = ParseResultFunc(parseFunc)
	DefaultClient.globalParseFunc = GlobalParseFunc
}

// html document
func Html(url string) (BytesLake, []byte, error) {
	return DefaultClient.Html(url)
}

// upload files
func Upload(url string, file *UploadFile, query ...*Query) BytesLake {
	if file == nil {
		return DefaultClient.UploadError(errors.New("upload file is nil"))
	}

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)

	fs, err := os.Open(file.FileName)
	if err != nil {
		return DefaultClient.UploadError(err)
	}
	part, err := writer.CreateFormFile(file.Name, filepath.Base(file.FileName))
	if err != nil {
		return DefaultClient.UploadError(err)
	}

	io.Copy(part, fs)
	fs.Close()

	for _, q := range query {
		if err := writer.WriteField(q.Key, q.Value); err != nil {
			return DefaultClient.UploadError(err)
		}
	}

	c := DefaultClient.Header("Content-Type", writer.FormDataContentType())

	if err := writer.Close(); err != nil {
		return DefaultClient.UploadError(err)
	}

	return c.Post(url, body)
}

// set timeout, million seconds
func Timeout(timeout int) *vrClient {
	return DefaultClient.Timeout(timeout)
}

// set cookie
// if cookie is nil, the cookies is set to empty
func SetCookies(rawUrl string, ck ...*http.Cookie) *vrClient {
	return DefaultClient.SetCookies(rawUrl, ck...)
}
